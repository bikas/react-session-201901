import React, { PureComponent } from 'react';
import './App.css';

class App extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      name: props.name || 'World!'
    };
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>
            Hello {this.state.name}
          </p>
        </header>
      </div>
    );
  }
}

export default App;
